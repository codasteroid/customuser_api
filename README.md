# CustomUser API

This is a Django project that demonstrates how to set up a custom user model, and build a RESTful API for managing posts and user accounts. The API also includes token-based user authentication and registration endpoints for user and control.

## Getting Started

These instructions will help you set up and run the project on your local development machine.

### Prerequisites

* Python (3.7+)
* pip (Python package manager)
* Virtual environment (optional but recommended)

### Installation

1.  Clone the project repository to your local machine:

```bash
git clone https://gitlab.com/codasteroid/customuser_api.git
```

2. Change your working directory to the project folder:

```bash
cd customuser_api
```

3. Create a virtual environment (optional but recommended):

```bash
python3 -m venv venv
```

4. Activate the virtual environment:

* On Linux and macOS:

```bash
source venv/bin/activate
```

* On Windows:

```bash
venv\Scripts\activate
```

5. Install project dependencies:

```bash
pip install -r requirements.txt
```

6. Apply database migrations to create the database schema:

```bash
python manage.py migrate
```

7. Start the development server: 

```bash
python manage.py runserver
```
The project should now be running locally. You can access the API endpoints at http://localhost:8000/api/.

## Authentication and User Management

The project provides a set of APIs for user authentication and management with the help of `dj-rest-auth` package:

* Password Reset:
	```bash
    POST /api/auth/password/reset/
    POST /api/auth/password/reset/confirm/
    ```

* User Authentication:
	```bash
    POST /api/auth/login/ 
    POST /api/auth/logout/    
    ```

* User Profile:
	```bash
    GET /api/auth/user/ 
    ```

* Password Change:
	```bash
    POST /api/auth/password/change/
    ```

* User Registration:

	```bash
    POST /api/registration/ 
    ```
## Posts Management

The project also includes APIs for managing posts:

* `GET /api/posts/` - Retrieve a list of posts.
* `POST /api/posts/` - Create a new post.
* `GET /api/posts/{post_id}/` - Retrieve a specific post.
* `PUT /api/posts/{post_id}/` - Update a post.
* `DELETE /api/posts/{post_id}/` - Delete a post.

These APIs allow users to create, read, update, delete, and filter posts.

## Testing

To run the project's tests, use the following command:

```bash
python manage.py test
```

## Contributing

If you'd like to contribute to this project, please follow these steps:

1. Fork the repository on GitLab.
2. Clone your fork locally.
3. Create a new branch for your feature or bug fix.
4. Make your changes and commit them.
5. Push your changes to your GitLab repository.
6. Create a pull request to the original repository.