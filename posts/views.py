from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .models import Post
from .serializers import PostSerializer
from .filters import PostFilterSet


class PostViewSet(viewsets.ModelViewSet):
    """
    A viewset for managing Post model instances using standard CRUD operations.
    """

    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated]
    filterset_class = PostFilterSet

    def get_queryset(self):
        """
        Define the queryset of Post model instances based on the current user.

        Returns:
            QuerySet: A queryset of Post model instances filtered by the current user.
        """
        user = self.request.user
        if user.is_authenticated:
            return Post.objects.filter(author=user)
        return Post.objects.none()
        """
        If the user is authenticated, return a queryset containing posts created by the user.
        If the user is not authenticated, return an empty queryset to deny access.
        """
