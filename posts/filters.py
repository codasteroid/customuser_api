from django_filters import rest_framework as filters
from django_filters.widgets import RangeWidget
from .models import Post


class PostFilterSet(filters.FilterSet):
    """A filter set for filtering Post model instances."""

    title = filters.CharFilter(field_name='title', lookup_expr='icontains')
    created_at = filters.DateFromToRangeFilter(
        widget=RangeWidget(attrs={'type': 'date'}))

    class Meta:
        model = Post
        # The fields from the Post model that can be used as filter parameters.
        fields = [
            'title',
            'created_at'
        ]
