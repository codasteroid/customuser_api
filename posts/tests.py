from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from users.models import CustomUser
from .models import Post
from .serializers import PostSerializer


class PostViewTestCase(APITestCase):
    """Test cases for the views and endpoints related to the Post model."""

    def setUp(self):
        """ Set up test data, including a test user account and a test post."""

        # Create a test account
        self.author = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='HR4TYbY53'
        )
        # Authenticate the test account
        self.client.force_authenticate(user=self.author)
        # Create a test post
        self.post = Post.objects.create(
            author=self.author,
            title='My first post',
            description='My first post description'
        )

    def test_get_queryset_authenticated_user(self):
        """Test retrieving a list of posts by an authenticated user."""
        url = reverse('posts-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = PostSerializer([self.post], many=True).data
        self.assertEqual(response.data, serializer_data)

    def test_get_queryset_unauthenticated_user(self):
        """Test retrieving a list of posts by an unauthenticated user."""
        self.client.logout()
        url = reverse('posts-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_post(self):
        """Test creating a new post."""
        url = reverse('posts-list')
        data = {
            'title': 'My second post',
            'description': 'My second post description'
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        post = Post.objects.get(title='My second post')
        self.assertEqual(self.author, post.author)

    def test_retrieve_post(self):
        """Test retrieving a specific post by its primary key."""
        url = reverse('posts-detail', args=[self.post.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = PostSerializer(self.post).data
        self.assertEqual(response.data, serializer_data)

    def test_update_post(self):
        """Test updating an existing post."""
        url = reverse('posts-detail', args=[self.post.pk])
        data = {
            'title': 'My first updated post',
            'description': 'My first updated description'
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        post = Post.objects.get(pk=self.post.pk)
        self.assertEqual(post.title, 'My first updated post')

    def test_delete_post(self):
        """Test deleting a post."""
        url = reverse('posts-detail', args=[self.post.pk])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Post.objects.filter(pk=self.post.pk).exists())
