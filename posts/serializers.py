from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    """
    A serializer for the Post model, allowing data serialization and deserialization 
    for use in RESTful APIs.
    """

    class Meta:
        model = Post
        # The fields from the Post model to be included in the serialized output.
        fields = [
            'id',
            'author',
            'title',
            'description',
            'created_at',
            'updated_at'
        ]
        # Fields that they can't be modified when creating or updating a post.
        read_only_fields = ['author']

    def create(self, validated_data):
        """
        Custom method to create a new post. Automatically sets the author to 
        the current user making the request.

        Args:
            validated_data (dict): The validated data for creating a post.

        Returns:
            Post: The newly created post instance.
        """

        validated_data['author'] = self.context['request'].user
        return super().create(validated_data)
