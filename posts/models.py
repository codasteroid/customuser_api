from django.db import models
from users.models import CustomUser


class Post(models.Model):
    """A model representing a post made by a user."""

    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a string representation of the post, which is its title."""

        return self.title
