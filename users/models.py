from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin


class CustomUserManager(BaseUserManager):
    """ A custom user manager class for creating and managing CustomUser instances."""

    def create_user(self, email, username, password):
        """
        Create and save a standard user with the given email, username, and password.

        Args:
            email (str): The user's email address.
            username (str): The user's username.
            password (str): The user's password.

        Returns:
            CustomUser: The created user instance.
        """
        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, username, password):
        """
        Create and save a superuser with the given email, username, and password.

        Args:
            email (str): The superuser's email address.
            username (str): The superuser's username.
            password (str): The superuser's password.

        Returns:
            CustomUser: The created superuser instance.
        """
        user = self.create_user(
            email=email,
            username=username,
            password=password
        )
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class CustomUser(AbstractBaseUser):
    """A custom user model that extends AbstractBaseUser"""

    email = models.EmailField(max_length=254, unique=True)
    username = models.CharField(max_length=30, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        """
        Return a string representation of the user.

        Returns:
            str: The username of the user.
        """
        return self.username

    class Meta:
        """Metadata options for the CustomUser model."""

        verbose_name_plural = 'users' # Sets the plural name to 'users'
