from django.db import IntegrityError
from django.test import TestCase
from .models import CustomUser


class CustomUserTestCase(TestCase):
    """A test case class for testing the CustomUser model and its manager."""

    def setUp(self):
        """Set up test user accounts for various test scenarios."""

        # Create a regular test user account
        self.user = CustomUser.objects.create_user(
            username='testuser',
            email='testuser@e.com',
            password='HR4TYbY57'
        )
        # Create a superuser test account
        self.superuser = CustomUser.objects.create_superuser(
            username='testsuperuser',
            email='testsuperuser@e.com',
            password='HR4TYbY53'
        )

    def test_customuser_string_representation(self):
        """Test the string representation of CustomUser instances."""
        self.assertEqual(str(self.user), 'testuser')
        self.assertEqual(str(self.superuser), 'testsuperuser')

    def test_regular_user_account_creation(self):
        """Test the creation and properties of a regular user account."""
        self.assertIsInstance(self.user, CustomUser)
        self.assertEqual(self.user.is_staff, False)
        self.assertEqual(self.user.is_superuser, False)
        self.assertEqual(self.user.is_active, True)

    def test_superuser_account_creation(self):
        """Test the creation and properties of a superuser account."""
        self.assertIsInstance(self.superuser, CustomUser)
        self.assertEqual(self.superuser.is_staff, True)
        self.assertEqual(self.superuser.is_superuser, True)
        self.assertEqual(self.superuser.is_active, True)

    def test_user_username_uniqueness(self):
        """Test that creating a user with a duplicate username raises an IntegrityError."""
        with self.assertRaises(IntegrityError):
            CustomUser.objects.create_user(
                username='testuser',
                email='regularuser@e.com',
                password='HR4TYbY56'
            )

    def test_user_email_uniqueness(self):
        """Test that creating a user with a duplicate email address raises an IntegrityError."""
        with self.assertRaises(IntegrityError):
            CustomUser.objects.create_user(
                username='regularuser',
                email='testuser@e.com',
                password='HR4TYbY59'
            )
